import os

from config import DATA_ROOT
from models import Request
from utils.shortcuts import make_response


def remove_directory(request: Request):
    if not os.path.exists(DATA_ROOT):
        return make_response("You should initialize DFS first")

    file_name = request.message.unary_action.file_name
    try:
        (DATA_ROOT / file_name).rmdir()
    except Exception as e:
        return make_response(str(e))

    request.app.redis.publish_remove_dir(file_name)

    return make_response("ok")
