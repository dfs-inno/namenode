import shutil

from config import DATA_ROOT
from models import Request
from utils.shortcuts import make_response


def move_file(request: Request):
    source_name = request.message.binary_action.source_name
    destination_name = request.message.binary_action.destination_name
    if not (DATA_ROOT / source_name).exists():
        return make_response("File doesn`t exists")

    shutil.move(DATA_ROOT / source_name, DATA_ROOT / destination_name)

    request.app.redis.publish_move(source_name, destination_name)
    return make_response("ok")
