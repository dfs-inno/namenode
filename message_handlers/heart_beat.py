import time

from models import Request
from utils.shortcuts import make_response


def heart_beat(request: Request):
    ip = request.message.heart_beat.ip
    request.app.heart_beats[ip] = time.time()
    return make_response("ok")
