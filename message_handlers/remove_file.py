import os

from config import DATA_ROOT
from models import Request
from utils.shortcuts import make_response


def remove_file(request: Request):
    file_name = request.message.unary_action.file_name
    if not (DATA_ROOT / file_name).exists():
        return make_response("File doesn`t exists")

    (DATA_ROOT / file_name).unlink()

    request.app.redis.publish_rm_file(file_name)
    return make_response("ok")
