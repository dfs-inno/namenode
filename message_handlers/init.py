import time

from models import Request
from utils import remove_store
from utils.shortcuts import make_response


def init(request: Request):
    status = remove_store()
    if status != "ok":
        return make_response(status)
    if not request.app.redis.get_all_storage_servers():
        return make_response("There are no storage servers available")
    request.app.redis.set_all_free_space(-1)
    request.app.redis.publish_remove_dir(".")
    free_space = -1
    while free_space == -1:
        free_space = request.app.redis.get_minimum_free_space()
        time.sleep(0.5)
    return make_response(str(int(free_space)))
