import math
import os
import random

from config import DATA_ROOT, CHUNK_SIZE
from models import Request, INode
from protos import nn_to_client_pb2
from utils.shortcuts import make_response


def write_file(request: Request) -> nn_to_client_pb2.StorageServerIp:
    if not os.path.exists(DATA_ROOT):
        return make_response("You should initialize DFS first")

    file_name = request.message.write_action.file_name
    size = request.message.write_action.size

    storage_servers = request.app.redis.get_all_storage_servers()
    if storage_servers:
        storage_server = random.choice(storage_servers)
    else:
        return make_response("There are no storage servers")
    if not (DATA_ROOT / file_name).exists():
        i_node = INode(name=file_name, size=size, num_of_blocks=math.ceil(size/CHUNK_SIZE))
        with open(DATA_ROOT / file_name, 'w') as f:
            f.write(i_node.json)
    response = nn_to_client_pb2.StorageServerIp()
    response.status = "ok"
    response.ip = storage_server
    return response
