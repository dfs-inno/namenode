from config import DATA_ROOT
from models import INode, Request
from protos import nn_to_client_pb2
from utils.shortcuts import make_response


def info_handler(request: Request):
    file_name = DATA_ROOT / request.message.unary_action.file_name
    if not file_name.exists():
        return make_response("File does not exists")
    with open(file_name) as f:
        data = f.read()
    i_node = INode.get_from_json(data)

    response = nn_to_client_pb2.INode()
    response.file_name = i_node.name
    response.size = i_node.size
    response.number_of_blocks = i_node.num_of_blocks
    ips = nn_to_client_pb2.StorageServersIps()
    available_servers = request.app.redis.get_all_storage_servers()
    ips.ips.extend(list(filter(lambda x: x in available_servers, i_node.servers)))
    response.ips.CopyFrom(ips)
    response.status = 'ok'
    return response
