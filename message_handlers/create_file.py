import os
from config import DATA_ROOT
from models import Request
from models.INode import INode
from utils.shortcuts import make_response


def create_file(request: Request):
    if not os.path.exists(DATA_ROOT):
        return make_response("You should initialize DFS first")

    file_name = request.message.unary_action.file_name

    if (DATA_ROOT / file_name).exists():
        (DATA_ROOT / file_name).touch()
        return make_response("ok")

    i_node = INode(name=file_name)
    with open(DATA_ROOT / file_name, 'w') as f:
        f.write(i_node.json)

    return make_response("ok")
