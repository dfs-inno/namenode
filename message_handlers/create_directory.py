import os

from config import DATA_ROOT
from models import Request
from utils.shortcuts import make_response


def create_directory(request: Request):
    if not os.path.exists(DATA_ROOT):
        return make_response("You should initialize DFS first")

    dir_name = request.message.unary_action.file_name

    try:
        os.mkdir(DATA_ROOT / dir_name)
    except Exception as e:
        return make_response(str(e))

    request.app.redis.publish_mkdir(dir_name)

    return make_response("ok")
