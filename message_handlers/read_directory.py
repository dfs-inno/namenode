import os

from config import DATA_ROOT
from models import Request
from protos import nn_to_client_pb2
from utils.shortcuts import make_response


def read_directory(request: Request):
    if not os.path.exists(DATA_ROOT):
        return make_response("You should initialize DFS first")

    file_name = request.message.unary_action.file_name
    try:
        data = os.listdir(DATA_ROOT / file_name)
    except Exception as e:
        return make_response(str(e))

    response = nn_to_client_pb2.ListFiles()
    response.status = 'ok'
    response.file[:] = data
    return response
