import json

from config import DATA_ROOT
from models import Request
from utils.read_all_files import read_all_files
from utils.shortcuts import make_response


def dump_file_system(request: Request):
    file_system = {
        "inodes": []
    }
    try:
        for _, inode in read_all_files():
            file_system["inodes"].append(json.loads(inode.json))
    except FileNotFoundError as e:
        pass
    return make_response(json.dumps(file_system))
