from config import DATA_ROOT
from models import Request, INode
from utils.shortcuts import make_response


def update_list_servers(request: Request):
    filename = request.message.update_file_ips.filename
    ip = request.message.update_file_ips.ip

    with open(DATA_ROOT / filename, 'r') as f:
        inode = INode.get_from_json(f.read())

    inode.servers.append(ip)
    with open(DATA_ROOT / filename, 'w') as f:
        f.write(inode.json)

    return make_response("ok")
