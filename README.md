# namenode

## How to run

### Install dependencies
```bash
pip install -r requirements.txt
pip install protobuf
```

### Environment variables
```bash
CHUNK_SIZE=1024 # Size of chunk, default value is 1024
REDIS_HOST=127.0.0.1 # Where is redis running
```

### Run without docker
```bash
export CHUNK_SIZE=1024
export REDIS_HOST=127.0.0.1
python main.py
```

### Run in docker container
```bash
docker run -p 5678:5678 -e CHUNK_SIZE=1024 -e REDIS_HOST=127.0.0.1 wselfjes/namenode
```

### Run in docker swarm
```bash
docker service create \
 -e REDIS_HOST=172.31.21.186 \
-e CHUNK_SIZE=1048576 \
--publish published=5678,target=5678,mode=host \
--name namenode \
wselfjes/namenode
```