import os
from pathlib import Path

from config import DATA_ROOT
from models import INode


def read_all_files(directory: Path = DATA_ROOT):
    for filename in os.scandir(directory):
        filename = Path(filename)
        if filename.is_dir():
            for file, inode in read_all_files(filename):
                yield file, inode
        if filename.is_file():
            with open(filename) as f:
                inode = INode.get_from_json(f.read())
            yield filename, inode
