from protos import nn_to_client_pb2


def make_response(message: str) -> nn_to_client_pb2.Response:
    message_response = nn_to_client_pb2.Response()
    message_response.status = message
    return message_response
