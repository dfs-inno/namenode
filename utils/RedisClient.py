import json

import redis

from config import REDIS_HOST, REDIS_PORT


class RedisClient:

    def __init__(self, redis_host=REDIS_HOST, redis_port=REDIS_PORT):
        self.__redis = redis.Redis(redis_host, redis_port)

    def add_new_storage_server(self, ip):
        return self.__redis.zadd("storage_ips", {ip: -1})

    def get_all_storage_servers(self):
        return [ip.decode('utf-8') for ip in self.__redis.zrange("storage_ips", 0, -1)]

    def set_all_free_space(self, value):
        ips = self.get_all_storage_servers()
        self.__redis.zadd("storage_ips", {ip: value for ip in ips})

    def get_minimum_free_space(self):
        storage_server = self.__redis.zrange("storage_ips", 0, 0, withscores=True)[0]
        return storage_server[1]

    def delete_storage_server(self, storage_ip):
        self.__redis.zrem("storage_ips", storage_ip)

    def publish_rm_file(self, file_name):
        self.__redis.publish("rm-channel", file_name)

    def publish_copy(self, source, destination):
        self.__redis.publish("cp-channel", json.dumps({"from": source, "to": destination}))

    def publish_move(self, source, destination):
        self.__redis.publish("mv-channel", json.dumps({"from": source, "to": destination}))

    def publish_remove_dir(self, file_name):
        self.__redis.publish("rmdir-channel", file_name)

    def publish_mkdir(self, file_name):
        self.__redis.publish("mkdir-channel", file_name)
