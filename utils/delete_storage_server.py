from utils import RedisClient
from utils.read_all_files import read_all_files


def delete_storage_server(storage_ip, redis):
    delete_from_inodes(storage_ip)
    delete_from_redis(storage_ip, redis)


def delete_from_inodes(storage_ip):
    for file, inode in read_all_files():
        if storage_ip in inode.servers:
            inode.servers.pop(inode.servers.index(storage_ip))
            if len(inode.servers) > 0:
                with open(file, 'w') as f:
                    f.write(inode.json)
            else:
                file.unlink()


def delete_from_redis(storage_ip: str, redis: RedisClient):
    redis.delete_storage_server(storage_ip)
