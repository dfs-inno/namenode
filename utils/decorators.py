

def logger_decorator(handler, logger):

    def wrapper(message):
        response = handler(message)
        input = str(message).replace("\n", " ")
        output = str(response).replace("\n", " ")
        logger.info(f"Input : {{ {input} }}, Output: {{ {output} }}")
        return response

    return wrapper
