import os
import shutil

from config import DATA_ROOT


def remove_store():

    if os.path.exists(DATA_ROOT):
        shutil.rmtree(DATA_ROOT)

    os.mkdir(DATA_ROOT)
    return 'ok'
