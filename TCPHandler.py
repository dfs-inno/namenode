import socketserver
import time
from threading import Thread

from models.Request import Request
from protos import client_to_nn_pb2
from utils.RedisClient import RedisClient
from utils.decorators import logger_decorator
from utils.shortcuts import make_response
from utils.delete_storage_server import delete_storage_server


def get_app(message_handlers, logger):
    redis = RedisClient()
    heart_beats = {}
    heart_beat_check_thread = Thread(target=heart_beat_check, args=(heart_beats, logger, redis))
    heart_beat_check_thread.start()

    class App(socketserver.BaseRequestHandler):

        def __init__(self, *args, **kwargs):
            self.redis = redis
            self.logger = logger
            self.heart_beats = heart_beats

            super(App, self).__init__(*args, **kwargs)

        def handle(self):
            length = self.request.recv(4)
            length = int.from_bytes(length, byteorder='big', signed=False)
            data = self.request.recv(length)

            message = client_to_nn_pb2.Command()
            message.ParseFromString(data)
            handler = logger_decorator(message_handlers.get(message.type, lambda x: None), self.logger)
            request = Request(message=message, app=self)
            response = handler(request)

            if response:
                response_data = response.SerializeToString()
            else:
                response_data = make_response("Incorrect message").SerializeToString()

            self.request.sendall(response_data)

    return App


def heart_beat_check(heart_beats, logger, redis):
    while True:
        to_remove = []
        for storage_ip in heart_beats.keys():
            if time.time() - heart_beats[storage_ip] > 30:
                logger.warning(f"Storage {storage_ip} server is down")
                delete_storage_server(storage_ip, redis)
                to_remove.append(storage_ip)

        [heart_beats.pop(storage_ip) for storage_ip in to_remove]
        time.sleep(5)
