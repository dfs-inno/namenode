from protos import client_to_nn_pb2
from message_handlers import info_handler, init, create_file, create_directory, \
    read_directory, remove_file, write_file, copy_file, move_file, remove_directory, heart_beat,\
    dump_file_system, update_list_servers


routes = {
    client_to_nn_pb2.Command.COMMAND_TYPE.INFO: info_handler,
    client_to_nn_pb2.Command.COMMAND_TYPE.INIT: init,
    client_to_nn_pb2.Command.COMMAND_TYPE.CREATE_FILE: create_file,
    client_to_nn_pb2.Command.COMMAND_TYPE.CREATE_DIRECTORY: create_directory,
    client_to_nn_pb2.Command.COMMAND_TYPE.READ_DIRECTORY: read_directory,
    client_to_nn_pb2.Command.COMMAND_TYPE.REMOVE_FILE: remove_file,
    client_to_nn_pb2.Command.COMMAND_TYPE.WRITE_FILE: write_file,
    client_to_nn_pb2.Command.COMMAND_TYPE.COPY_FILE: copy_file,
    client_to_nn_pb2.Command.COMMAND_TYPE.MOVE_FILE: move_file,
    client_to_nn_pb2.Command.COMMAND_TYPE.REMOVE_DIRECTORY: remove_directory,
    client_to_nn_pb2.Command.COMMAND_TYPE.HEART_BEAT: heart_beat,
    client_to_nn_pb2.Command.COMMAND_TYPE.DUMP_FILE_SYSTEM: dump_file_system,
    client_to_nn_pb2.Command.COMMAND_TYPE.UPDATE_LIST_SERVERS: update_list_servers,
}
