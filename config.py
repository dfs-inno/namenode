import os
from pathlib import Path
import logging


LOGGER_LEVEL = logging.WARNING

CHUNK_SIZE = int(os.environ.get("CHUNK_SIZE", 1024))

HOST, PORT = "0.0.0.0", 5678
DATA_ROOT = Path('./data/')

REDIS_HOST = os.environ.get("REDIS_HOST", "127.0.0.1")
REDIS_PORT = 6379
