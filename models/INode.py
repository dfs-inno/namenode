import json
from dataclasses import dataclass, field
from typing import List


@dataclass
class INode:
    name: str
    size: int = 0
    num_of_blocks: int = 0
    servers: List[str] = field(default_factory=list)

    @property
    def json(self):
        return json.dumps({
            "name": self.name,
            "size": self.size,
            "num_of_blocks": self.num_of_blocks,
            "servers": self.servers
        })

    @classmethod
    def get_from_json(cls, json_string: str):
        json_ = json.loads(json_string)
        return cls(**json_)
